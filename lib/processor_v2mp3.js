/*
 *		Video 2 MP3 processor
 *
 *		(http://www.video2mp3.at/)
 *
 *
 *
 *		Processor steps:
 *			[1] access "http://www.video2mp3.at/settings.php?set=check&format=mp3&id="+youtube_id+"&key=3500000" // NOTE: key calculation (currently, Math.floor(Math.random() * 3500000)) may change
 *			[2] split result by "|"
 *				[2.1] result[0] -> status ("OK", "ERROR", "DOWNLOAD")
 *				[2.a] result[0] == "OK" ?
 *							[1] -> server
 *							[2] -> request id
 *							[3] -> title
 *							[4] -> YES / NO ??
 *					[2.a.1] generate download link: 'http://s' + [1] + '.video2mp3.at/dl.php?id=' + [2]
 *
 *
 *		Limitations and other considerations:
 *			- currently, there is a video length limit of 20 min
 *			- some videos may be restricted by copyright reasons	
 *			- download links expire and so they cannot be cached
 *
 */

var request = require('request');

var got = function(f,data){
	console.log("//debug: got "+JSON.stringify(data));
	if(f==undefined){
		console.log("\t // skipping callback function call for data "+JSON.stringify(data));
	}else if(typeof f === 'function'){
		f(data);
	}else{
		throw new Error("ERROR: could not invoke function -- unexpected type: "+(typeof f));
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	VIDEO2MP3.at
//
//

V2MP3ProcessStatus = function(videoId){
	this.videoId=videoId;
	this.title=null;
	this.status='unknown';
	this.info={};
	this.downloadUrl=null;
	this.percentage = null;
}


V2MP3ProcessStatus.prototype.setTitle = function(title){
	this.title=title;
	return this;
}

V2MP3ProcessStatus.prototype.setReady = function(downloadUrl){
	this.status='ready';
	this.downloadUrl=downloadUrl;
	this.percentage = 100;
	return this;
}

V2MP3ProcessStatus.prototype.setDownloading = function(percentage){
	if (this.percentage === null) {
		this.percentage = 0;
	}

	if (percentage !== null && percentage !== undefined){
		this.percentage = percentage;
	}

	this.status='downloading';

	return this;
}

V2MP3ProcessStatus.prototype.isReady = function(){
	return this.status=='ready';
}

V2MP3ProcessStatus.prototype.getDownloadUrl = function(){
	return this.downloadUrl;
}

V2MP3ProcessStatus.prototype.getInfo = function(){
	return this.info;
}

V2MP3ProcessStatus.prototype.addInfo = function(key, info){
	this.info[key]=info;
	return this;
}





Video2MP3 = function(){
	this.domain='video2mp3.at';
	this.protocol='http://';
	this.serviceUrl=this.protocol+'www.'+this.domain;

}

Video2MP3.prototype.processResult = function(result, data, success, error) {
	var status = result[0];

	if (status == "OK") {
		data.addInfo('okDate', new Date());
		var server = result[1];
		var dlId = result[2];
		var dlTitle = result[3];
		var downloadUrl = this.protocol+ 's' + server + '.' +this.domain+ '/dl.php?id=' + dlId;
		data.addInfo('downloadUrl', downloadUrl).setTitle(dlTitle).setReady(downloadUrl);
		got(success, data);
	/* istanbul ignore next */
	}else if (status == "DOWNLOAD") {
		data.addInfo('lastDownloadingDate', new Date());
		var dlTitle = result[1];
		var dlPercentage = result[2];
		data.setTitle(dlTitle).setDownloading(parseFloat(dlPercentage));
		got(success, data);
	/* istanbul ignore next */
	} else if (status == "ERROR" && result[1]=="PENDING") {
		data.addInfo('pendingDate', new Date()).setDownloading();
		got(success, data);
	} else {
		got(error, "Could not process video. Error details: ["+result.join(',')+"]; data info: "+JSON.stringify(data.getInfo()));
	}
}

Video2MP3.prototype.check = function(videoId,success,error) {
	var key = Math.floor(Math.random() * 3500000);
	var checkBase = '/settings.php?set=check&format=mp3&key='+key+'&id=';
	var checkUrl = this.serviceUrl+checkBase+videoId;
	var $this = this;

	request(checkUrl,function(err,resp,body){
		if(err){
			got(error,err);
		}else{
			var data = new V2MP3ProcessStatus(videoId);
			data.addInfo('originalResult', body);
			var result = (body||'ERROR').split('|');
			$this.processResult(result, data, success, error);
		}
	});



	return this;
};




//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	NODE.JS MODULE EXPORTS
//
// 


var V2MP3_INSTANCE = new Video2MP3();

module.exports = {
	
	//Check for download link
	check: function(videoId, success, error){
		V2MP3_INSTANCE.check(videoId, success, error);
	},
	// Check if process status is ready to download
	isReady: function(data){
		return data && data.isReady && data.isReady()
	},
	// Retrieve a download URL for a data that is ready
	getDownloadUrl: function(data){
		return data && data.isReady && data.isReady() && data.getDownloadUrl ?
					data.getDownloadUrl() : null;
	}


}


