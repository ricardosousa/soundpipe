/*
 *		Youtube API wrapper
 *
 *
 */

var http = require('http');


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	YOUTUBE QUERY BUILDING
//
//

YouTubeQuery = function(){
	this.baseUrl='http://gdata.youtube.com/feeds/api/videos';
	this.attrs={v:2,alt:'json','start-index':1,'max-results':20,fields:'entry(title,media:group(media:player))'};
}

YouTubeQuery.prototype.query = function(arg,encode) {
	var arg = encode===undefined||encode?encodeURIComponent((arg||'').trim()):(arg||'').trim();
	if(arg.length) this.attrs.q=arg;
	else delete this.attrs.q;
	return this;
};

YouTubeQuery.prototype.offset = function(idx) {
	if(idx && idx>=1)this.attrs['start-index']=idx;
	return this;
}

YouTubeQuery.prototype.category = function(category) {
	if(category && category.trim().length)this.attrs.category=category.trim();
	else delete this.attrs.category;
	return this;
}

YouTubeQuery.prototype.maxResults = function(results) {
	if(results && results>=1)this.attrs['max-results']=results;
	return this;
}

YouTubeQuery.prototype.prettyPrint = function(pp) {
	this.attrs.prettyprint=pp===undefined?true:!!pp;
	return this;
}

YouTubeQuery.prototype.attr = function(param,value) {
	if(!arguments.length) return this;
	if(arguments.length<2 || !param) return param?this.attrs[param]:null;
	if(value===undefined) delete this.attrs[param]; //accept null values
	else this.attrs[param] = value;
	return this;
}

YouTubeQuery.prototype.remove = function(param) {
	this.attr(param,undefined);
}

YouTubeQuery.prototype.buildUrl = function() {
	var params = [];
	for(var a in this.attrs){
		if(this.attrs[a]!=undefined) params.push(a+'='+this.attrs[a]);
	}
	return params.length?this.baseUrl+'?'+params.join('&'):this.baseUrl;
}

YouTubeQuery.prototype.execute = function(success,error) {
	var url = this.buildUrl();
	//console.log('[querying youtube API with URL "'+url+'"]');
	http.get(url, function(res) {
		var body = '';
		res.on('data', function(chunk) { body += chunk });
		res.on('end', function() { success(JSON.parse(body)) });
	}).on('error', error);
}

YouTubeQuery.prototype.getUrls = function(success,error) {
	var $this = this;
	this.execute(function(data){
		
		if(!data || !data.feed || !data.feed.entry){
			error('could not retrieve data feed entries for query term "'+$this.attrs.q+'"" (retrieved response: '+JSON.stringify(data)+')');
		}else{
			var urls = [];
			data.feed.entry.forEach(function(e){
				urls.push(e['media$group']['media$player'].url);
			});
			success(urls);
		}
	},error);
}




//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	YOUTUBE METADATA BUILDING
//
//

/*
 *
 *	Metadata structure
 *
 *
	 {
		title:'entry[].title.$t',
		author:'entry[].author[].+(name.$t)',
		description:'entry[].media$group.media$description.$t',
		id:passed video Id,
		url:'entry[].media$group.media$player.url',
		thumbnails:'entry[].media$group.media$thumbnail[]./( /default$/i.test(.yt$name) -> {.url,.height,.width})',
		duration:'entry[].media$group.yt$duration.seconds',
		views:'entry[].yt$statistics.viewCount',
		likes:'entry[].yt$rating.numLikes',
		dislikes:'entry[].yt$rating.numDislikes'
	}
 *
 */



var extractMetadata = function(id,from,success,error){
	if(!id || !from || !from.entry){
		error('missing video metadata entry');
	}else{
		var md = {
			id : id
		};
		var v = from.entry;

		if(v.title && v.title['$t']) md.title=v.title['$t'];
		if(v.author && v.author.length) md.author = v.author.map(function(a){ if(a.name && a.name['$t']) return a.name['$t'] }).join(', ');
		if(v['media$group']){
			var media = v['media$group'];
			if(media['media$player'] && media['media$player'].url) md.url=media['media$player'].url;
			if(media['media$thumbnail'] && media['media$thumbnail'].length)
				md.thumbnails=media['media$thumbnail'].filter(function(t){ return /default/i.test(t['yt$name']) }).map(function(t){ return {url:t.url,height:t.height,width:t.width} });
			if(media['yt$duration'] && media['yt$duration'].seconds)md.duration = media['yt$duration'].seconds;
		}
		if(v['yt$statistics'] && v['yt$statistics'].viewCount) md.views = v['yt$statistics'].viewCount;
		if(v['yt$rating'] && v['yt$rating'].numLikes ) md.likes = v['yt$rating'].numLikes;
		if(v['yt$rating'] && v['yt$rating'].numDislikes ) md.dislikes = v['yt$rating'].numDislikes;

		success(md);
	}
}

var doGetMeta = function(id,success,error) {
	

	var baseUrl = 'http://gdata.youtube.com/feeds/api/videos/';
	var params = '?v=2&alt=json';

	http.get(baseUrl + id + params , function(res) {
		var body = '';
		res.on('data', function(chunk) { body += chunk });
		res.on('end', function() { extractMetadata(id, JSON.parse(body),success,error) });
	}).on('error', error);

}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	NODE.JS MODULE EXPORTS
//
// 


module.exports = {
	
	// Get youtube query object
	query: function(query,encode){
				return new YouTubeQuery().query(query,encode);
			},

	// Extract an array of youtube URLs from given text (or an empty array if could be found)
	extract: function(text){
				return ((text||'').match(/\b([^\s]*youtube\.[a-z]{2,6}\/[^\s]+|[^\s]*youtu\.be\/[^\s]+)\b/ig)||[]);
			},

	// Get one canonical URL from given text (or null, if none)
	canonical: function(text){
				var src = module.exports.extract(text)[0],id=module.exports.id(src);
				return id?'http://www.youtube.com/watch?v='+id:null;
			},

	id: function(url){
			if(!url)return null;
			var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
			var match = url.match(regExp);
			if (match&&match[2].length==11){
			    return match[2];
			}else{
			    return null;
			}
		},

	metadata: function(id,success,error){
				if((id||'').length!=11){
					error('unexpected video id ("'+id+'")');
				}else{
					doGetMeta(id,success,error);
				}
			 }
}


