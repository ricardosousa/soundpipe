
/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/ 
/*												IMPORTS								 			  */



var request = require('request');
var cheerio = require('cheerio');
var youtube = require('./youtube_api');




/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/ 
/*											AUX FUNCTIONS							 			  */


var got = function(f,data){
	if(f==undefined){
		console.log("\t // skipping callback function call for data "+JSON.stringify(data));
	}else if(typeof f === 'function'){
		f(data);
	}else{
		throw new Error("ERROR: could not invoke function -- unexpected type: "+(typeof f));
	}
}


var reqPage = function(url,success,error){
	request(url,function(err,resp,body){
		if(err){
			got(error,err);
		}else{
			got(success,cheerio.load(body));
		}
	});
}


var redirectPage = function(url,success,error,count){
	var count = count || 1;
	request({ url: url, followRedirect: false }, function (err, resp, body) {
				if(err){
					got(error,err);
				}else {
					var pUrl = resp.request.response.headers.location;
					if(pUrl==undefined || pUrl==url){
						got(success,url);
					}else if(count>=20){
						got(error,"20 redirects limit exceeded");
					}else{
						redirectPage(pUrl,success,error,count++);
					}
				}
			});
}



/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/ 
/*										BASE URL SERVICE SCRAPPER					 			  */


function BaseUrlService(cleanRegex,buildResult,description){
	this.description=description;
	this.cleanRegex=cleanRegex;
	this.buildResult=buildResult;
}

BaseUrlService.prototype.clean = function(url){
 	return ((url||'').match(this.cleanRegex)||[])[0];
 }

BaseUrlService.prototype.match = function(url){
 	return !!this.clean(url);
 }

BaseUrlService.prototype.prepare = function(url,success,error){
	if(!this.match(url)){
		got(error,'String "'+url+'" does not contain a'+(this.description?' '+this.description:'n')+' URL');
	}else{
		var $this = this;
		reqPage($this.clean(url),function($){
			$this.buildResult($,success,error);
		},error);
	}
}


/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/ 
/*										YOUTUBE SERVICE REFLECTOR					 			  */


function YouTube(){}

YouTube.prototype.clean = function(url){
 	return youtube.extract(url)[0];
}

YouTube.prototype.match = function(url){
 	return !!this.clean(url);
}

YouTube.prototype.prepare = function(url,success,error){
	if(!this.match(url)){
		got(error,'String "'+url+'" does not contain a YouTube URL');
	}else{
		got(success,youtube.extract(url).map(youtube.canonical));
	}
}


/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/ 
/*										SOUNDHOUND SCRAPPER							 			  */


function SoundHound(){
 	BaseUrlService.call(this,
 						/\bhttp[^\s]+soundhound\.com[^\s]+\b/i,
 						function($,success){ got(success,[$("embed").attr("src")]) },
 						'SoundHound');
}

SoundHound.prototype = Object.create(BaseUrlService.prototype);

SoundHound.prototype.constructor = SoundHound;




/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/ 
/*											SHAZAM SCRAPPER							 			  */


function Shazam(){
 	BaseUrlService.call(this,
 						/\bhttp[^\s]+(shz\.am|shazam\.com)[^\s]+\b/i,
 						function($,success,error){

 							var qTitle = $("title").text();
 							if(!qTitle.length){
 								got(error,'Could not retrieve music title from Shazam info page');
 							}else{
 								youtube.query(qTitle).category('Music').maxResults(3).getUrls(success,function(ytError){
 									got(error,'Could not search youtube videos for Shazam query: '+ytError);	
 								});
 							}
 						},
 						'Shazam');
}

Shazam.prototype = Object.create(BaseUrlService.prototype);

Shazam.prototype.constructor = Shazam;



/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/ 
/*											SPOTIFY SCRAPPER						 			  */


function Spotify(){
	
 	BaseUrlService.call(this,//only spotify tracks are supported for now
 						/\bhttp[^\s]+spotify[^\s]+track[^\s]+\b/i); 
}

Spotify.prototype = Object.create(BaseUrlService.prototype);

Spotify.prototype.constructor = Spotify;

// @Override
Spotify.prototype.prepare = function(url,success,error){
	if(!this.match(url)){
		got(error,'String "'+url+'" does not contain a Spotify URL');
	}else{
		var $this = this;
		request('http://ws.spotify.com/lookup/1/.json?uri='+encodeURIComponent($this.clean(url)),function(err,resp,data){

			var retrieved = undefined;
			try{
				retrieved = JSON.parse(data).track;
			}catch(pError){
				retrieved=null;
			}
			if(err){
				got(error,err);
			}else if(!retrieved || !retrieved.name || !retrieved.artists || !retrieved.artists.length){
				got(error,'Could not retrieve song info from Spotify API');
			}else{
				youtube.query(retrieved.artists.map(function(e){return e.name}).join(' ')+' - '+retrieved.name).category('Music').maxResults(3).getUrls(success,function(ytError){
					got(error,'Could not search youtube videos for Spotify query: '+ytError);	
				});
			}
		});
	}
}


/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/ 
/*									GOOGLE PLAY MUSIC SCRAPPER						 			  */


function GooglePlayMusic(){
 	BaseUrlService.call(this,
 						/\bhttp[^\s]+play\.google\.com\/store\/music[^\s]+\b/i,
 						function($,success,error){

 							var $title=$(".title[itemprop='name']");
 							var sTitle=$title.text();
 							var sArtist=$title.parent().parent().find(".artist[itemprop='name']").text();
 							var qTerm = null;
 							if(!sTitle || !sArtist){//probably one artist album
 								var wrds =[];
 								$("[itemprop='name']").each(function(x,v){if(v.content) wrds.push(v.content)});
 								if(sTitle) wrds.push(sTitle);
 								qTerm = wrds.join(' ');

 							}else{
 								qTerm=sArtist+' - '+sTitle;
 							}

 							if(!qTerm || !qTerm.length){
 								got(error,'Could not retrieve music details from Google Play Music info page');
 							}else{
 								youtube.query(qTerm).category('Music').maxResults(3).getUrls(success,function(ytError){
 									got(error,'Could not search youtube videos for Google Play Music query: '+ytError);	
 								});
 							}
 						},
 						'GooglePlayMusic');
}

GooglePlayMusic.prototype = Object.create(BaseUrlService.prototype);

GooglePlayMusic.prototype.constructor = GooglePlayMusic;



/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/ 
/*										GROOVESHARK SCRAPPER						 			  */


function GrooveShark(){
 	BaseUrlService.call(this,
 						/\bhttp[^\s]+grooveshark\.com[^\s]+\b/i,
 						function($,success,error){

 							var qTitle = $('h1').first().text();
 							if(!qTitle.length){
 								got(error,'Could not retrieve music title from GrooveShark info page');
 							}else if(/Grooveshark/i.test(qTitle)){
 								got(error,'Could not parse result from GrooveShark info page (retrieved title was "'+qTitle+'")');
 							}else{
 								youtube.query(qTitle).category('Music').maxResults(3).getUrls(success,function(ytError){
 									got(error,'Could not search youtube videos for GrooveShark query: '+ytError);	
 								});
 							}
 						},
 						'GrooveShark');
}

GrooveShark.prototype = Object.create(BaseUrlService.prototype);

GrooveShark.prototype.constructor = GrooveShark;

GrooveShark.prototype._superClean=GrooveShark.prototype.clean;

//@Override
GrooveShark.prototype.clean = function(url){
	return this._superClean((url||'').replace('grooveshark.com/#!/','grooveshark.com/'));
}


//---------


/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/ 
/*											RDIO SCRAPPER							 			  */


function Rdio(){
	
 	BaseUrlService.call(this,//only rdio tracks are supported for now..
 						/\bhttp[^\s]+rd(io\.com|\.io)[^\s]+\b/i); 
}

Rdio.prototype = Object.create(BaseUrlService.prototype);

Rdio.prototype.constructor = Rdio;

// @Override
Rdio.prototype.prepare = function(url,success,error){

	//Helper function for retrieving partial info from a Rdio URL
	function rdioInfo(from,what){
		var found = (from||'').match(new RegExp("\/"+what+"\/([^\/]+)","i"));
		return found?found[1]:'';
	}

	if(!this.match(url)){
		got(error,'String "'+url+'" does not contain a Rdio URL');
	}else{
		
		redirectPage(this.clean(url), function (pUrl) {
	
			var qArtist = rdioInfo(pUrl,"artist");
			var qTrack = rdioInfo(pUrl,"track");

			if(!qTrack.length){
				got(error,'Could not retrieve music title from Rdio info page');
			}else if(!qArtist.length){
				got(error,'Could not retrieve music artist from Rdio info page');
			}else{
				youtube.query(qArtist + " - " + qTrack).category('Music').maxResults(3).getUrls(success,function(ytError){
					got(error,'Could not search youtube videos for Rdio query: '+ytError);	
				});
			}
	
			
		},error);

	}
}




/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/ 
/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/ 
/*										NODEJS CORE EXPORTS							 			  */
/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/ 
/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/ 


var moduleServices = [
						{id:'SoundHound', instance:new SoundHound(), quickmatches:['soundhound']},
						{id:'Shazam', instance:new Shazam(), quickmatches:['shz.am','shazam.com']},
						{id:'GooglePlayMusic', instance: new GooglePlayMusic(), quickmatches:['play.google.com/store/music']},
						{id:'GrooveShark', instance:new GrooveShark(), quickmatches:['grooveshark']},
						{id:'YouTube', instance:new YouTube(), quickmatches:['youtube.','youtu.be']},
						{id:'Spotify', instance:new Spotify(), quickmatches:['spotify']},
						{id:'Rdio', instance:new Rdio(), quickmatches:['rd.io','rdio.com']}
					];

var getModuleServicesFor = function(url){
	var url = (url || '').toLowerCase(), res = [];
	
	moduleServices.filter(function(each){
							 return each.quickmatches.reduce(function(p,c){ return p || url.indexOf(c)>=0 },false);
						 }).forEach(function(service){
							if(service.instance.match(url)) res.push(service);
						});


	if(!res.length) console.log('\t * WARN * no known service capable of handling "'+url+'"');
	
	return res;
}

module.exports = {

	list: 		function(){  return moduleServices.reduce(function(p,c){ return p.concat(c.id) },[])  },

	know: 		function(url){  return !!getModuleServicesFor(url).length  },

	handlers: 	function(url){  return getModuleServicesFor(url).reduce(function(p,c){ return p.concat(c.id) },[])  },

	prepare: 	function(url,success,error){
					var services = getModuleServicesFor(url);
					if(!services.length){
						got(error,'No known services capable of handling requested data');	
					}else{
						var remaining = services.length, result = [],errors = [];
						
						var updateStatus = function(){
							if(--remaining <= 0){ 
								if(result.length){
									got(success,result);
								}else {
									got(error,'All known services for requested data failed to preprocess it. Error(s) details: '+errors.join('; '));
								}
							}	
						};
						var handleServiceSuccess = function(data){  result=result.concat(data); updateStatus()  };
						var handleServiceError = function(data){  errors=errors.concat(data); updateStatus()  };

						services.forEach(function(each){ each.instance.prepare(url,handleServiceSuccess,handleServiceError) });
						
					}
				}


}



/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/ 

