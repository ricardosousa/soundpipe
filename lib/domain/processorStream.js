/*
 * Wrapper for a processor that transforms an youtube metadata object or a videoId string to processor result
 */


var Transform = require('stream').Transform;
var util = require('util');


util.inherits(ProcessorStream, Transform);


function ProcessorStream(processor, options) {

	var options = options || {};
	options.objectMode = true;

	if (!(this instanceof ProcessorStream))
    return new ProcessorStream(options);

	this._processor=processor;
	this.skipErrors= options && options.skipErrors;
	Transform.call(this, options);
}

ProcessorStream.prototype._decode = function(data, encoding, callback) {
	return data && data.id ? data.id : data;
}

ProcessorStream.prototype._encode = function(data, encoding, callback) {
	return data;
}

ProcessorStream.prototype._transform = function(data, encoding, callback) {
	var $this = this;
	console.log("//debug [ProcessorStream] transforming: "+$this._decode(data));
	var $callback = function(error, data) {
		if($this.skipErrors && error){
			console.log("skipping error: "+error);
			error = null;
		}
		if (error) {
			console.error("an error occurred: "+error);
		}
		if(arguments.length==2){
		callback(error,data);
		}else if(arguments.length==1){
			callback(error);
		}else{
			callback();
		}
	}

	if(!$this._processor) {
		$callback('No processor defined');
	}
	$this._processor.check($this._decode(data), function(result){
		console.log("success! "+JSON.stringify(result));
		$callback(null, $this._encode(result));
	}, $callback);
};

ProcessorStream.prototype._flush = function(callback) {
	callback();
}


module.exports = ProcessorStream;