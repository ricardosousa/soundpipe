/*
 * Wrapper for service handlers that transforms a query string to video Ids, falling back to a youtube query when no handlers can process given query
 */


var Transform = require('stream').Transform;
var youtubeQueryStream = require('./youtubeQueryStream');
var util = require('util');
var es = require('event-stream');


util.inherits(ServicesStreamWithFallback, Transform);


function ServicesStreamWithFallback(services, youtube, options) {

	var options = options || {};
	options.objectMode = true;

	if (!(this instanceof ServicesStreamWithFallback))
    return new ServicesStreamWithFallback(options);

	this._services = services;
	this._youtube = youtube;
	this._youtubeQueryStream = new youtubeQueryStream(youtube,options);

	this.skipErrors = options && options.skipErrors;

	Transform.call(this, options);
}

ServicesStreamWithFallback.prototype._decode = function(data, encoding, callback) {
	return data;
}

ServicesStreamWithFallback.prototype._encode = function(data, encoding, callback) {
	return data;
}

ServicesStreamWithFallback.prototype._transform = function(data, encoding, callback) {
	var $this = this;

	console.log("//debug [ServicesStreamWithFallback] transforming: "+$this._decode(data));

	var $callback = function(error, data) {
		console.log("//debug [ServicesStreamWithFallback]: got "+JSON.stringify(data));
		if($this.skipErrors && error){
			console.log("skipping error: "+error);
			error = null;
		}
		if (error) {
			console.error("an error occurred: "+error);
		}
		if(arguments.length==2){
		callback(error,data);
		}else if(arguments.length==1){
			callback(error);
		}else{
			callback();
		}
	}

	if(!$this._youtube) {
		$callback('No youtube API defined');
	}else if(!$this._services){
		$callback('No services instance defined');
	}else{
		var term = $this._decode(data);


		var handleResult = function(result){

console.log(result)
			if(!result || !result.length){
				$callback('No result retrieved');
			} else {
				for(var i=0;i<result.length;i++){
	  				var toPush = $this._youtube.id(result[i]) || result[i];
	  				console.log("//debug [ServicesStreamWithFallback] pushing "+toPush);
	  				$this.push($this._encode(toPush));
	  			}
				$callback();
			}
		}

		$this._services.prepare(term, handleResult,
									  function(onFallBack){
									  //Fallback to youtube searchs
									  	es.readArray([term])
									  	  .pipe($this._youtubeQueryStream)
									  	  .pipe(es.writeArray(function (err, array){
									  	  	  if(err) {
									  	  	  	$callback(err);
									  	  	  }else{
									  	  	  	handleResult(array);	
									  	  	  }
										    }));
									 });
	}

};


ServicesStreamWithFallback.prototype._flush = function(callback) {
	callback();
}

module.exports = ServicesStreamWithFallback;