/*
 * Wrapper for an youtube api instance that transforms youtube query terms to video Ids
 */


var Transform = require('stream').Transform;
var util = require('util');


util.inherits(YoutubeQueryStream, Transform);


function YoutubeQueryStream(youtube, options) {

	var options = options || {};
	options.objectMode = true;

	if (!(this instanceof YoutubeQueryStream))
    return new YoutubeQueryStream(options);

	this._youtube = youtube;
	this.skipErrors = options && options.skipErrors;
	this.category = options && options.category ? options.category : 'Music';
	this.maxResults = options && options.maxResults ? options.maxResults : 1;
	Transform.call(this, options);
}

YoutubeQueryStream.prototype._decode = function(data, encoding, callback) {
	return data;
}

YoutubeQueryStream.prototype._encode = function(data, encoding, callback) {
	return data;
}

YoutubeQueryStream.prototype._transform = function(data, encoding, callback) {
	var $this = this;

	console.log("//debug [YoutubeQueryStream] transforming: "+$this._decode(data));

	var $callback = function(error, data) {
		console.log("//debug [YoutubeQueryStream]: got "+JSON.stringify(data));
		if($this.skipErrors && error){
			console.log("skipping error: "+error);
			error = null;
		}
		if (error) {
			console.error("an error occurred: "+error);
		}
		if(arguments.length==2){
		callback(error,data);
		}else if(arguments.length==1){
			callback(error);
		}else{
			callback();
		}
	}

	if(!$this._youtube) {
		$callback('No youtube API defined');
	} else{
		$this._youtube.query($this._decode(data))
					  .category($this.category)
					  .maxResults($this.maxResults)
					  .getUrls(function(result){
					  			for(var i=0;i<result.length;i++){
					  				var toPush = $this._youtube.id(result[i]);
					  				console.log("//debug [YoutubeQueryStream] pushing "+toPush);
					  				$this.push($this._encode(toPush));
					  			}
								$callback();
							   }, $callback);
	}
};

YoutubeQueryStream.prototype._flush = function(callback) {
	callback();
}


module.exports = YoutubeQueryStream;