/*
 * Wrapper for an youtube api instance that transforms an youtube video Id to metadata
 */


var Transform = require('stream').Transform;
var util = require('util');


util.inherits(YoutubeMetadataStream, Transform);


function YoutubeMetadataStream(youtube, options) {

	var options = options || {};
	options.objectMode = true;

	if (!(this instanceof YoutubeMetadataStream))
    return new YoutubeMetadataStream(options);

	this._youtube = youtube;
	this.skipErrors = options && options.skipErrors;
	Transform.call(this, options);
}

YoutubeMetadataStream.prototype._decode = function(data, encoding, callback) {
	return data;
}

YoutubeMetadataStream.prototype._encode = function(data, encoding, callback) {
	return data;
}

YoutubeMetadataStream.prototype._transform = function(data, encoding, callback) {
	var $this = this;
	var $callback = function(error, data) {
		if($this.skipErrors && error){
			console.log("skipping error: "+error);
			error = null;
		}
		if(arguments.length==2){
		callback(error,data);
		}else if(arguments.length==1){
			callback(error);
		}else{
			callback();
		}
	}

	if(!$this._youtube) {
		$callback('No youtube API defined');
	}

	$this._youtube.metadata($this._decode(data), function(result){
		if(!result){
			$callback('No result retrieved');
		}else{
			$callback(null, $this._encode(result));
		}
	}, $callback);
};

YoutubeMetadataStream.prototype._flush = function(callback) {
	callback();
}


module.exports = YoutubeMetadataStream;