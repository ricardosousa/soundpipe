

var express = require('express');
var services = require('./lib/services');
var youtube = require('./lib/youtube_api');
var v2mp3Processor= require('./lib/processor_v2mp3');
var processorStream = require('./lib/domain/processorStream');
var youtubeQueryStream = require('./lib/domain/youtubeQueryStream');
var youtubeMetadataStream = require('./lib/domain/youtubeMetadataStream');
var servicesStreamWithFallback = require('./lib/domain/servicesStreamWithFallback');
var es = require("event-stream");
var bodyParser = require('body-parser');
var path = require('path');

var app = express();

app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, 'web-ui')));



var lastApiRequest = new Date();
var maxTimeRequests = 60; //about one per second in one minute interval
var currentRequests = 0;
var timeWindow = 60000;
var waitFor = null;


var lastError = null;
var errorDate = null;



function isAuthorized(req,res){
	var now=new Date();
	currentRequests++;
	if(now < waitFor) return false;

	if(now - lastApiRequest >= timeWindow){
		lastApiRequest=now;
		currentRequests=1;
		waitFor=null;
	}else if(currentRequests>=maxTimeRequests){
		waitFor=waitFor||(lastApiRequest+timeWindow);
		return false;
	}

	return true;
}

function saveError(msg){
	lastError=msg;
	errorDate=new Date();
}

app.all('*', function(req, res, next) {
  // res.header("Access-Control-Allow-Origin", "*");
  // res.header("Access-Control-Allow-Headers", "X-Requested-With");

  if(!isAuthorized(req,res)){
		console.error('!!! Max requests exceeded for timewindow parameters !!!');
		console.log('Details:');
		console.log('- req.ip : '+req.ip);
		console.log('- req.originalUrl : '+req.originalUrl);
		console.log('- last api request window : '+lastApiRequest.toString());
		res.send(550);
	}else{
	  next();
	}
 });

app.get('/api/v1_0/metadata/:videoId', function(req, res){

	req.connection.setTimeout(3000);

	es.readArray([req.param('videoId')])
				.pipe(new youtubeMetadataStream(youtube,{skipErrors:true}))
				.pipe(es.writeArray(function(err,array){
				 res.send(array)
				  }));

});

app.get('/api/v1_0/query', function(req, res){

	req.connection.setTimeout(15000);

	var term = null;
	if(req.param('q')) term = decodeURIComponent(req.param('q'));

	es.readArray([term])
				.pipe(new servicesStreamWithFallback(services,youtube,{maxResults:12,skipErrors:true}))
				.pipe(es.writeArray(function(err,array){
				 res.send(array)
				  }));

});

app.get('/api/v1_0/process/:videoId', function(req, res){

	req.connection.setTimeout(3000);

	es.readArray([req.param('videoId')])
				.pipe(new processorStream(v2mp3Processor,{skipErrors:true}))
				.pipe(es.writeArray(function(err,array){
				 res.send(array)
				  }));

});


/**
 *
 * Deprecated
 *
 */
app.get('/api/old', function(req, res){

	req.connection.setTimeout(3000);

	var term = null;
	if(req.param('q')) term = decodeURIComponent(req.param('q'));
	else if(req.param('m')) term = decodeURIComponent(req.param('m'));
	else if(req.param('d')) term = decodeURIComponent(req.param('d'));
	else if(req.param('qd')) term = decodeURIComponent(req.param('qd'));

	if(term)console.log("got request for query: "+term);

	if(!isAuthorized(req,res)){
		console.error('!!! Max requests exceeded for timewindow parameters !!!');
		console.log('Details:');
		console.log('- req.ip : '+req.ip);
		console.log('- req.originalUrl : '+req.originalUrl);
		console.log('- last api request window : '+lastApiRequest.toString());
		res.send(550);
	}else if(req.param('e')){
		res.send({lastError:lastError,errorDate:errorDate});
	}else if(req.param('qd')){
		services.prepare(term,function(urls){
			if(!urls || !urls.length) res.send(404,{error:'no services for given parameter'});
			else res.redirect(urls[0]); //FIXME
		},function(error){
			//Fallback to youtube searchs

			es.readArray([term])
				.pipe(new youtubeQueryStream(youtube,{maxResults:1,skipErrors:true}))
				.pipe(new youtubeMetadataStream(youtube,{skipErrors:true}))
				.pipe(new processorStream(v2mp3Processor,{skipErrors:true}))
				.pipe(es.writeArray(function(err,array){ res.send(array) }));
		});
	}else if(req.param('q')){
		services.prepare(term,function(urls){
			if(!urls || !urls.length) res.send(404,{error:'no services for given parameter'});
			else res.redirect(urls[0]);
		},function(error){
			//Fallback to youtube searchs
			youtube.query(term).category('Music').maxResults(1).getUrls(
					function(youtubeURL){
						if(!youtubeURL || !youtubeURL.length) res.send(404,{error:'no services for given parameter'});
						else res.redirect(youtubeURL[0]);
					},function(ytError){
						saveError(ytError);
						res.send(500,ytError);
					});
		});
	}else if(req.param('d')){
		services.prepare(term,function(urls){
			if(!urls || !urls.length) res.send(404,{error:'no services for given parameter'});
			else es.readArray([youtube.id(urls[0])])
				    .pipe(new processorStream(v2mp3Processor,{skipErrors:true}))
					.pipe(es.writeArray(function(err, array){
							res.send(array.map(function(x){ return x.toString() }));
							//saveError(e);res.send(500,e)
						}));
		},function(error){
			//No Fallback
			saveError(error);
			res.send(500,error);
		});
	}else if(term){
		if(youtube.id(term) || term.length==11){
			youtube.metadata(youtube.id(term) || term,function(r){res.send(r)},function(e){saveError(e);res.send(500,e)});
		}else{
			services.prepare(term,function(urls){
				if(!urls || !urls.length) res.send(404,{error:'no services for given parameter'});
				else youtube.metadata(youtube.id(urls[0]),function(r){res.send(r)},function(e){saveError(e);res.send(500,e)});
			},function(error){
				//Fallback to youtube searchs
				youtube.query(term).category('Music').maxResults(1).getUrls(
						function(youtubeURL){
							if(!youtubeURL || !youtubeURL.length) res.send(404,{error:'no services for given parameter'});
							else youtube.metadata(youtube.id(youtubeURL[0]),function(r){res.send(r)},function(e){saveError(e);res.send(500,e)});
						},function(ytError){
							saveError(ytError);
							res.send(500,ytError);
						});
			});
		}
	}else{
		res.send(400);
	}
});



app.listen(3333);


console.log(" ~  ~   ~  ~   ~  ~  LISTENING ON PORT 3333  ~  ~   ~  ~   ~  ~  ");