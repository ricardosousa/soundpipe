- - - -

# Environment Requirements #
## Required Apps ##

Server platform:

* [Node.js](http://nodejs.org/)

Web UI:

* [Bower](http://bower.io/)

## Project dependecies ##

Service dependencies:

* On the root directory, run:
    * `$ npm install`

Frontend dependencies:

* On the web-ui directory, run:
    * `$ bower install`

- - - -

# Testing and coverage #

## Dependencies ##

* mocha (globally):
    * `$ npm install -g mocha`
* istanbul (globally):
    * `$ npm install -g istanbul`

## Running tests ##
On the root directory, run:

* `$ mocha`

## Coverage ##
On the root directory, run:

* `$ istanbul cover _mocha`
* `$ open coverage/lcov-report/index.html`

- - - -