
var assert = require("assert");
var processor = require('../lib/processor_v2mp3.js');


// ---// ---// ---// ---// ---// ---// ---// ---



describe("--- VIDEO2MP3 TEST SUITE --- ", function () {
	describe("Generic tests", function () {

		it('should load processor module',function(){
			assert.ok(processor!=null && processor!=undefined);
		});
		
	});



	// ---// ---// ---// ---// ---// ---// ---// ---


	var checkWithRetries = function(retries, delay, resultTest, end) {

		var iteration = 1;

		var action = function(err){

			var currentIteration = iteration;
			iteration++;

			if(err){
				console.log("[WARNING] Failed run nº "+currentIteration+" : "+err);
			}

			if(currentIteration > retries) {
				end("Exceeded maximum retries");
				return;
			}

			processor.check(resultTest.target, function(result){

				assert.notEqual(result.getInfo(), null, 'result info shouldn\'t ever be null');

				if(processor.isReady(result)){
					assert.notEqual(processor.getDownloadUrl(result), null, 'shouldn\'t have a null download URL');
					assert.equal(result.videoId, resultTest.target, 'should have same youtube videoId');
					assert.equal(result.percentage, 100, 'should have concluded conversion');
					end();
				}else{
					assertEqual(processor.getDownloadUrl(result), null, 'should have null DownloadUrl if result is not yet ready');
					console.log("[DEBUG] Not yet ready; waiting "+delay+"ms");
					setTimeout(function(){
						console.log("[DEBUG] Retrying for data: "+JSON.stringify(result));
						action();	
					},delay);
				}
			},action);
			
		};

		action();
	}


	describe("Result tests", function () {

		this.timeout(20000);

		var resultTestSet = [
								{target:'DdC6f8hBo6s'},
								{target:'p5ATO2Tds8A'},
								{target:'LJ-vin0tSEE'}
							];


		resultTestSet.forEach(function(resultTest){


			it('should be able to retrieve a download link for "'+resultTest.target+'"', function(done){
				checkWithRetries(5,2000,resultTest,done);
			});
		});

	});




	// ---// ---// ---// ---// ---// ---// ---// ---



});
