
var assert = require("assert");
var ProcessorStream = require('../../lib/domain/processorStream.js');
var es = require("event-stream");

// ---// ---// ---// ---// ---// ---// ---// ---

var MockedProcessor = function(checkers){

	this.check = function(videoId, success, error){
		var result = checkers[videoId];
		if (result) {
			success({
				videoId : videoId,
				status : 'ready',
				downloadUrl : 'dl:'+result
			});
		} else {
			error(videoId);
		}
	}

	this.isReady = function(data){
		return data && data.status == 'ready';
	}

	this.getDownloadUrl = function(data){
		return data ? data.downloadUrl : null;
	}
}

// ---// ---// ---// ---// ---// ---// ---// ---



describe("--- PROCESSOR STREAM TEST SUITE --- ", function () {
	describe("Generic tests", function () {

		it('should load ProcessorStream module',function(){
			assert.ok(ProcessorStream!=null && ProcessorStream!=undefined);
		});
		
	});



	// ---// ---// ---// ---// ---// ---// ---// ---




	describe("Result tests", function () {

		this.timeout(3000);

		var processor = new MockedProcessor({
				'A' : '1',
				'B' : '2'
			});

		it('should process expected data', function(done){

			var stream = new ProcessorStream(processor);

            es.readArray(['B','A','A'])
            	.pipe(stream)
            	.pipe(es.writeArray(function(err, array){
            		assert.equal(
            			''+array.map(function(x){ return x.toString() }),
            			'dl:2,dl:1,dl:1');
            		done();
            	}));

		});


		it('should fail on unexpected data', function(done){

			var stream = new ProcessorStream(processor);

			stream.on('error', function(error){
				assert.equal(error,'C');
				done();
			});
            es.readArray(['B','C','A'])
            	.pipe(stream)
            	.pipe(es.writeArray(function(err, array){
            		done('shouldn\'t get here');
            	}));


		});

		it('should ignore unexpected data when preference is set', function(done){

			var stream = new ProcessorStream(processor, {skipErrors:true});

            es.readArray(['A','C','B'])
            	.pipe(stream)
            	.pipe(es.writeArray(function(err, array){
            		assert.equal(
            			''+array.map(function(x){ return x.toString() }),
            			'dl:1,dl:2');
            		done();
            	}));

		});



	});




	// ---// ---// ---// ---// ---// ---// ---// ---



});
