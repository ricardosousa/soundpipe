
var assert = require("assert");
var services = require('../lib/services.js');


// ---// ---// ---// ---// ---// ---// ---// ---



describe("--- SERVICES.JS TEST SUITE --- ", function () {
	describe("Generic tests", function () {

		it('should load services module',function(){
			assert.ok(services!=null && services!=undefined);
		});
		
		it('should list at least on service',function(){
			assert.ok(services.list()!=null && services.list().length);
		});

		it('should gracefully handle null/undefined/unkown service queries',function(){
			assert.ok(!services.know(null));
			assert.ok(!services.know(undefined));
			assert.ok(!services.know());
			assert.ok(!services.know('    '));
		});

		it('should fail undefined service prepare',function(done){
			services.prepare(undefined, done, function(error){ assert.equal(error,'No known services capable of handling requested data'); done(); } );
		});
	});




	// ---// ---// ---// ---// ---// ---// ---// ---




	describe("Soundhound core tests", function () {

		this.timeout(3000);

		var soundhoundSet = [
							'http://www.soundhound.com/?t=100716554850339889',
							'♬ Acabámos de encontrar Tsunami (Original Mix) do(s) Dvbbs & Borgeous em #SoundHound para Andróide.\nhttp://www.soundhound.com/?t=d56c10a94d5641daa0b3fc2ab9409a92'
						];


		soundhoundSet.forEach(function(soundhoundTest){

			it('should be able to handle soundhound core case tests ('+soundhoundTest+')', function(){
				assert.ok(services.know(soundhoundTest));
			});

			it('should be able to list handlers for soundhound core case tests ('+soundhoundTest+')', function(){
				assert.ok(services.handlers(soundhoundTest).length);
			});
			

			it('should be able to prepare core case test for "'+soundhoundTest+'"',function(done){
				services.prepare(soundhoundTest, function(result){ assert.ok(result && result.length); done(); }, done );
			});
		});

	});




	// ---// ---// ---// ---// ---// ---// ---// ---




	describe("Result tests", function () {

		var resultTestSet = [
								{target:'http://www.soundhound.com/?t=100716554850339889',result:'http://www.youtube.com/v/rQ_bFCiM7OE&hl=en_US&fs=1&'},
								{target:'Acabei de usar o Shazam para descobrir a Talento Clandestino de Dealema. http://shz.am/t40409156',result:'http://www.youtube.com/watch?v=O6G7-Pxx6wo&feature=youtube_gdata_player'},
								{target:'https://play.google.com/store/music/album?id=Beiwhdmitrj6do2iefocch2j7lq&tid=song-Tobnsadq73hz7gdsnk5oyup3fse',result:'http://www.youtube.com/watch?v=v1qYXMM-Bhw&feature=youtube_gdata_player'},
								{target:'https://play.google.com/store/music/album?id=B23xrrduim3qv6dqtacoxmf275y&tid=song-T5cn6qorpodlzmom2eh3ue6wlky',result:'http://www.youtube.com/watch?v=86khmc6y1yE&feature=youtube_gdata_player'},
								{target:'http://grooveshark.com/#!/s/Smile+Like+You+Mean+It/39Q52e?src=5',result:'http://www.youtube.com/watch?v=l1p_NHFd8jM&feature=youtube_gdata_player'},
								{target:'http://youtu.be/qrdpliMfoAM',result:'http://www.youtube.com/watch?v=qrdpliMfoAM'},
								{target:'//www.youtube.com/embed/qrdpliMfoAM',result:'http://www.youtube.com/watch?v=qrdpliMfoAM'},
								{target:'https://play.spotify.com/track/1pKYYY0dkg23sQQXi0Q5zN',result:'http://www.youtube.com/watch?v=s9MszVE7aR4&feature=youtube_gdata_player'},
								{target:'http://grooveshark.com/#!/s/Every+Little+Thing+She+Does+Is+Magic/2LkZ6M?src=5',result:'http://www.youtube.com/watch?v=aENX1Sf3fgQ&feature=youtube_gdata_player'},
								{target:'http://grooveshark.com/#!/s/By+The+Way/3XT814?src=5',result:'http://www.youtube.com/watch?v=JnfyjwChuNU&feature=youtube_gdata_player'},
								{target:'http://grooveshark.com/s/By+The+Way/3XT814?src=5',result:'http://www.youtube.com/watch?v=JnfyjwChuNU&feature=youtube_gdata_player'},
								{target:'http://rd.io/x/Rl5Nz5crUCZ1sg/',result:'http://www.youtube.com/watch?v=dGghkjpNCQ8&feature=youtube_gdata_player'},
								{target:'https://www.rdio.com/artist/Calvin_Harris/album/Now_28/track/Feel_So_Close/',result:'http://www.youtube.com/watch?v=dGghkjpNCQ8&feature=youtube_gdata_player'},
								{target:'http://www.shazam.com/discover/track/123453088', result:'http://www.youtube.com/watch?v=lp-EO5I60KA&feature=youtube_gdata_player'}
							];


		resultTestSet.forEach(function(resultTest){

			it('should return "'+resultTest.result+'" as first result (for "'+resultTest.target+'")',function(done){
				this.timeout(6000);
				services.prepare(resultTest.target, function(result){ assert.deepEqual([resultTest.result],result.slice(0,1)); done(); }, done );
			});
		});

	});




	// ---// ---// ---// ---// ---// ---// ---// ---



});
