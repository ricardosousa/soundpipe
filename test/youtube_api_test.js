
var assert = require("assert");
var youtube = require('../lib/youtube_api.js');





describe("--- YOUTUBE_API.JS TEST SUITE --- ", function () {


	// ---// ---// ---// ---// ---// ---// ---// ---


	describe("Generic tests", function () {

		it('should load youtube module',function(){
			assert.ok(youtube!=null && youtube!=undefined);
		});
		
	});


	// ---// ---// ---// ---// ---// ---// ---// ---
	// 	QUERY BUILDING TESTS

	var base='http://gdata.youtube.com/feeds/api/videos'
	var defparams='?v=2&alt=json&start-index=1&max-results=20&fields=entry(title,media:group(media:player))';


	describe("Simple query build tests", function () {

		
		it('should return basic query url without any changes',function(){
			assert.equal(youtube.query().buildUrl(), base+defparams);
		});

		it('should set individual parameters',function(){
			assert.equal(youtube.query('test').buildUrl(), base+defparams+'&q=test');
			assert.equal(youtube.query().offset(3).buildUrl(), base+'?v=2&alt=json&start-index=3&max-results=20&fields=entry(title,media:group(media:player))');
			assert.equal(youtube.query().category('test').buildUrl(), base+defparams+'&category=test');
			assert.equal(youtube.query().maxResults(74).buildUrl(), base+'?v=2&alt=json&start-index=1&max-results=74&fields=entry(title,media:group(media:player))');
			assert.equal(youtube.query().prettyPrint(true).buildUrl(), base+defparams+'&prettyprint=true');
			assert.equal(youtube.query().prettyPrint(false).buildUrl(), base+defparams+'&prettyprint=false');

		});

		it('should trim query',function(){
			assert.equal(youtube.query(' boo ').buildUrl(),base+defparams+'&q=boo');
		});

		it('should set batch of parameters',function(){
			assert.equal(youtube.query('        say what').maxResults(3).prettyPrint(false).category('Music').buildUrl(),
						 base+'?v=2&alt=json&start-index=1&max-results=3&fields=entry(title,media:group(media:player))&q=say%20what&prettyprint=false&category=Music');
		});
	});

	describe("Complex query build tests", function () {

		it('should support replacing attributes',function(){
			assert.equal(youtube.query('woot').category('I').category('dont').query('think I').category('know').buildUrl(),base+defparams+'&q=think%20I&category=know');
		});

		it('should remove certain invalid attributes',function(){
			assert.equal(youtube.query('test').query('     ').buildUrl(),base+defparams);
			assert.equal(youtube.query('test').query().buildUrl(),base+defparams);
			assert.equal(youtube.query('test').query(null).buildUrl(),base+defparams);
			assert.equal(youtube.query('test').query(undefined,false).buildUrl(),base+defparams);
			assert.equal(youtube.query().category('test').category(null).buildUrl(),base+defparams);
		});

		it('should suport sequencial set/unset actions',function(){
			assert.equal(youtube.query().maxResults(99).offset(1).maxResults(-5).maxResults(20).maxResults().buildUrl(),base+defparams);
			assert.equal(youtube.query('a').query().query('b').query().query('c').buildUrl(),base+defparams+'&q=c');
		});

		it('should ignore invalid ranges',function(){
			assert.equal(youtube.query().offset(undefined).maxResults(null).buildUrl(),base+defparams);
			assert.equal(youtube.query().offset(0).maxResults(-5).buildUrl(),base+defparams);
		});

		it('should not reencode already encoded query string',function(){
			assert.equal(youtube.query('what%20am%20I%20doing',false).buildUrl(),base+defparams+'&q=what%20am%20I%20doing');
		});

	});

	// ---// ---// ---// ---// ---// ---// ---// ---
	// URL PARSING TESTS

	describe("URL parsing tests", function () {


		var testSet = ['http://www.youtube.com/v/0zM3nApSvMg?fs=1&amp;hl=en_US&amp;rel=0',
					   'http://www.youtube.com/embed/0zM3nApSvMg?rel=0',
					   'www.youtube.com/watch?v=0zM3nApSvMg&feature=feedrec_grec_index',
					   'http://www.youtube.com/watch?v=0zM3nApSvMg',
					   'http://youtu.be/0zM3nApSvMg',
					   'youtu.be/un94Q8e7FOA',
					   'https://www.youtube.com/watch?v=0zM3nApSvMg#t=0m10s',
					   'http://www.youtube.com/user/IngridMichaelsonVEVO#p/a/u/1/QdK8U-VIH_o'];

		it('should be able to extract all urls from given text',function(){

			var from = "http://www.youtube.com/v/0zM3nApSvMg?fs=1&amp;hl=en_US&amp;rel=0 asas 143 l24??1=2 weewkj 2.cwmo com http://www.youtube.com/embed/0zM3nApSvMg?rel=0 welkw www.youtube.com/watch?v=0zM3nApSvMg&feature=feedrec_grec_index _____ http://www.youtube.com/watch?v=0zM3nApSvMg _____ http://youtu.be/0zM3nApSvMg we youtu.be/un94Q8e7FOA wlkej weijwel https://www.youtube.com/watch?v=0zM3nApSvMg#t=0m10s _____ http://www.youtube.com/user/IngridMichaelsonVEVO#p/a/u/1/QdK8U-VIH_o";
			assert.deepEqual(youtube.extract(from),testSet);
		});

		it('should retrieve ids from test set', function(){
			//id
			var ids = ['0zM3nApSvMg','0zM3nApSvMg','0zM3nApSvMg','0zM3nApSvMg','0zM3nApSvMg','un94Q8e7FOA','0zM3nApSvMg','QdK8U-VIH_o'];

			assert.deepEqual(testSet.map(youtube.id),ids);

		});

		it('should aid on building a canonical URL list from a string',function(){
			//canonical
			var text = 'video qq youtu.be/un94Q8e7FOA ;focos position muito bom https://www.youtube.com/watch?v=o4ivhP_OZV4&feature=c4-overview&list=UUnYo1CIiNdI-wSrirABalnA\n COMO COLOCAR PAINEL LED EM WYSIWYG http://www.youtube.com/watch?v=kOMyhbQIcaA&list=TLms7Uhtp3_88';
			var canonicals = ['http://www.youtube.com/watch?v=un94Q8e7FOA','http://www.youtube.com/watch?v=o4ivhP_OZV4','http://www.youtube.com/watch?v=kOMyhbQIcaA'];
			assert.deepEqual(youtube.extract(text).map(youtube.canonical),canonicals);
		});

	});



	// ---// ---// ---// ---// ---// ---// ---// ---
	// URL PARSING TESTS

	describe("Video metadata test (may change later)", function () {

		var testSet = [
							{
								target: 'http://www.youtube.com/watch?v=s9MszVE7aR4',
								result: {
											"id":"s9MszVE7aR4",
											"title":"Daft Punk - Around The World",
											"author":"emimusic",
											"url":"http://www.youtube.com/watch?v=s9MszVE7aR4&feature=youtube_gdata_player",
											"thumbnails":[
															{"url":"http://i.ytimg.com/vi/s9MszVE7aR4/default.jpg","height":90,"width":120},
															{"url":"http://i.ytimg.com/vi/s9MszVE7aR4/mqdefault.jpg","height":180,"width":320},
															{"url":"http://i.ytimg.com/vi/s9MszVE7aR4/hqdefault.jpg","height":360,"width":480}
														],
											"duration":"244",
											"views":"44585659",
											"likes":"216979",
											"dislikes":"5626"
										} 
							}
					  ];

		testSet.forEach(function(test){
			it('should retrieve video metadata for '+test.target,function(done){

				youtube.metadata(youtube.id(test.target),function(retrieved){
					//changing values:
					assert.ok(+retrieved.views>=+test.result.views,'number of views is greater or equal to previous value');
					retrieved.views = test.result.views;
					retrieved.likes = test.result.likes;
					retrieved.dislikes = test.result.dislikes;
					//assertion
					assert.equal(JSON.stringify(retrieved),JSON.stringify(test.result));
					done();
				},done);

			});	
		});
		

	});
	
});

// ---// ---// ---// ---// ---// ---// ---// ---


// ---// ---// ---// ---// ---// ---// ---// ---

