#!/bin/bash

#
#	Auxiliary script to download, build, install and configure MongoDB on a raspberry.
#	Please note that this script will take several hours to finish, give at least 8h hours for it to complete execution.
#
#
# 	script steps source: http://c-mobberley.com/wordpress/index.php/2013/10/14/raspberry-pi-mongodb-installation-the-working-guide/
#
#	start mongodb: $ sudo /etc/init.d/mongod start
#
#	connect to instance: $ mongo
#				- server running on http://localhost:27017 
#				- basic interface accessible via http://localhost:28017 
#					- see script steps source for instructions on how to enable REST interface for mongod
#

# ----- NOTE: ROOT REQUIRED ----- 

if [ "$(whoami)" != "root" ]; then
	echo "Sorry, you are not root."
	exit 1
fi


# ----- VARIABLES ----- 
#
#Target directory for mongodb download
TARGET_DIR="/home/pi"





# ----- Install required mongodb build libs ----- 

echo "   - - - - - - [ 1 ] Installing required libs... - - - - - - "
apt-get install build-essential libboost-filesystem-dev libboost-program-options-dev libboost-system-dev libboost-thread-dev scons libboost-all-dev python-pymongo git

# ----- Download, compile and install mongodb from source ----- 

echo "   - - - - - - [ 2 ] Cloning mongodb source... - - - - - - "
cd $TARGET_DIR
git clone https://github.com/skrabban/mongo-nonx86
cd mongo-nonx86

echo "   - - - - - - [ 3 ] Compiling and installing mongodb... - - - - - - "
# NOTE: these two commands will probably take several hours to finish each (give about 8h to finish execution)
scons
scons --prefix=/opt/mongo install


# ----- General housekeeping ----- 

echo "   - - - - - - [ 4 ] Setting up permissions and directories... - - - - - - "
adduser --firstuid 100 --ingroup nogroup --shell /etc/false --disabled-password --gecos "" --no-create-home mongodb
mkdir /var/log/mongodb/
chown mongodb:nogroup /var/log/mongodb/
mkdir /var/lib/mongodb
chown mongodb:nogroup /var/lib/mongodb

echo "   - - - - - - [ 5 ] Preparing init.d script and symlinks... - - - - - - "
cp debian/init.d /etc/init.d/mongod
cp debian/mongodb.conf /etc/
ln -s /opt/mongo/bin/mongod /usr/bin/mongod
ln -s /opt/mongo/bin/mongo /usr/bin/mongo
chmod u+x /etc/init.d/mongod
update-rc.d mongod defaults

echo "   - - - - - - Finished! - - - - - - "

